#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Mon Mar 12 17:33:21 2018

@author: Norman Roy de Guia
'''    
import re
import argparse
import time

def main(isa_file,asm_file,data_dump_file,inst_dump_file,
        max_instructions,verbose):

    start_time = time.time()    
    print 'Starting assembler...'
    # Read isa file
    with open(isa_file, 'r') as isa:
        isa_arr = [i.split(' ') for i in isa]
    isa.close()
    is_count = len(isa_arr)
    print 'Total Instruction Set lines: {0}'.format(is_count)     

    # Read assembly file
    with open(asm_file, 'r') as asm:
        asm_arr = [i.split(' ') for i in asm]
    asm.close()
    asm_count = len(asm_arr)
    print 'Total Instruction Set lines: {0}'.format(asm_count) 

    # Convert to binary first, then concatenate
    # Then convert to hex and store in hex_dump
    instdump = [0 for x in range(asm_count)]
    datadump = [0 for x in range(len(asm_arr[1]))]
    idump = open(inst_dump_file,'w')
    ddump = open(data_dump_file,'w')

    print '................................'.format()

    # main conversion loop
    # converts data first
    # then loops to convert all instructions
    # that are part of the isa file
    for x in range(asm_count):
        temp = ''
        dtemp = ''               
        if asm_arr[x][0] == '.data\n':
            convert_data_to_hex(x,dtemp,asm_arr,datadump,verbose)
        else:
            for y in range(is_count):
                if asm_arr[x][0] == isa_arr[y][0]:
                    convert_inst_to_hex(x,y,temp,asm_arr,isa_arr,instdump,verbose)
                    generate_solution(x,asm_arr)

    # FIX THIS PART
    # PATCH ONLY APPLIES WHEN DATA IN .data
    # consists of only 1 line of entries
    # this removes the first 4 entries taken up 
    # by data mem and all the spaces
    # find a new way to fix this
    instdump = instdump[4:]

    # This part formats the instdump to RISC-V 
    # specification compliant format
    instdump = ''.join(str(i) for i in instdump)
    datadump = ''.join(str(i) for i in datadump)

    # fills the output files with max_instructions
    # the x4 is here because 1 inst = 4 lines
    instdump = instdump + '0'*(max_instructions*4-len(instdump))
    datadump = datadump + '0'*(max_instructions*4-len(datadump))

    iteration_inst = iter(instdump)
    iteration_data = iter(datadump)

    instdump = '\n'.join(a+b for a,b in zip(iteration_inst,iteration_inst))
    datadump = '\n'.join(a+b for a,b in zip(iteration_data,iteration_data))

    # write to dump file
    idump.write(instdump)
    ddump.write(datadump)

    idump.close()
    ddump.close()

    # shell output
    total_time = (time.time() - start_time)/100.0
    if (verbose): print '................................'.format()    
    print 'Assembly done.'
    print 'Total time: {0}'.format(elapsed_time(time.time() - start_time))
    print 'Data file: {0}'.format(data_dump_file)
    print 'Inst file: {0}'.format(inst_dump_file)
    print '-- populated by {0}'.format(max_instructions)

def elapsed_time(sec):
    # this function returns string converted
    # from the time elapsed, units are adjusted
    # for more readable output
    if sec < 1: return str(sec*1000) + ' usec'
    elif sec < 60: return str(sec) + ' sec'
    elif sec < (60*60): return str(sec/60) + ' min'
    else: return str(sec/(60*60)) + ' hr'           

def convert_data_to_hex(x,temp,asm_arr,datadump,verbose):
    # converts data entries to binaries
    # then finally converts to hex and dumps in
    # ddump: datamem.txt            
    if (verbose):
        print 'Decoding datamem: {0}'.format(asm_arr[x])        
        print 'Number of datamem entries: {0}'.format(len(asm_arr[x+1]))
    for y in range(len(asm_arr[x+1])):
        if (verbose): print 'Convertng datamem entry: {0}'.format(asm_arr[x+1][y].strip(',\n'))
        if (int(asm_arr[x+1][y].strip(',\n'),10)) > 0:
            temp = str.zfill(bin(int(asm_arr[x+1][y].strip(',\n'),10))[2:],32)
        else: # two's complement for negative numbers
            n_int = int(asm_arr[x+1][y].strip(',\n'),10)
            temp = (bin(n_int & 0b11111111111111111111111111111111))
        datadump[y] = '{0:08X}'.format(int(temp,2))    
        if (verbose): print '> hex: 0x{0}'.format(datadump[y])
    if (verbose): print '................................'.format()       

def convert_inst_to_hex(x,y,temp,asm_arr,isa_arr,instdump,verbose):
    if (verbose): print 'Current Line to Decode: {0}'.format(asm_arr[x])
    temp = ''        
    # Do the operations here
    if asm_arr[x][0] in ('LW','SW'):
        # very bad code just to accomodate mips style coding
        # coding includes () to specify offset
        imm = str.zfill(bin(int(re.sub('[\(\[].*?[\)\]]','',asm_arr[x][2]),16))[2:],16)
        temp += imm[:12] # Imm field 31:25
        temp += str.zfill(bin(int(asm_arr[x][2][7:].strip('$R)\n'),10))[2:],5)
        temp += imm[12:] # Imm field 14:11
        temp += str.zfill(bin(int(asm_arr[x][1].strip('$R\n'),10))[2:],5)
        temp += (isa_arr[y][6]).strip('\n') # Opcode
    elif asm_arr[x][0] in ('ADDI','SLTI'):
		imm = str.zfill(bin(int(asm_arr[x][3],16))[2:],16)
		temp += imm[:12] # Imm field 31:25
		temp += str.zfill(bin(int(asm_arr[x][2].strip("$R\n"),10))[2:],5)
		temp += imm[12:] # Imm field 14:11
		temp += str.zfill(bin(int(asm_arr[x][1].strip("$R\n"),10))[2:],5)
		temp += (isa_arr[y][6]).strip("\n") # Opcode
    elif asm_arr[x][0] in ('ADD','SUB','SLT'):
        temp += isa_arr[y][1]
        temp += str.zfill(bin(int(asm_arr[x][3].strip('$R\n'),10))[2:],5)
        temp += str.zfill(bin(int(asm_arr[x][2].strip('$R\n'),10))[2:],5)
        temp += '{0:04b}'.format(0) # Don't cares
        temp += str.zfill(bin(int(asm_arr[x][1].strip('$R\n'),10))[2:],5) # RD
        temp += (isa_arr[y][6]).strip('\n') # Opcode
    elif asm_arr[x][0] in ('BEQ','BNE'):
		imm = str.zfill(bin(int(asm_arr[x][3],16))[2:],16)
		temp += imm[:7]
		temp += str.zfill(bin(int(asm_arr[x][2].strip('$R\n'),10))[2:],5) # RS2
		temp += str.zfill(bin(int(asm_arr[x][1].strip('$R\n'),10))[2:],5) # RS1
		temp += imm[7:] # RD
		temp += (isa_arr[y][6]).strip('\n') # Opcode      
    elif asm_arr[x][0] in ('J','JAL'):
        imm = str.zfill(bin(int(asm_arr[x][1],16))[2:],26)                
        temp += imm
        temp += (isa_arr[y][6]).strip('\n') # Opcode
    elif asm_arr[x][0] in ('JR'):
        temp += isa_arr[y][1]
        temp += '{0:014b}'.format(0) # Don't cares
        temp += str.zfill(bin(int(asm_arr[x][1].strip('$R\n'),10))[2:],5) # RD                
        temp += (isa_arr[y][6]).strip('\n') # Opcode
    elif asm_arr[x][0].strip('\n') in ('NOP','NOP\n'):
        imm = '1'*32
        temp += imm
    instdump[x] = '{0:08X}'.format(int(temp,2))
    if (verbose):
		print '> bin: {0}'.format(' '.join(temp[i:i+4] for i in xrange(0,32,4)))
		print '> hex: 0x{0}'.format(instdump[x])

def generate_solution(x,asm_arr):
    # function to generate solutions for testbench
    inst = asm_arr[x][0].strip('\n')
    if inst in ('LW'): pass

if __name__ == '__main__': #main()
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--isa_file',type = str,
        help = 'Use custom isa file',
        default = 'riscv.isa'
    )
    parser.add_argument(
        '--asm_file',type = str,
        help = 'Use custom assembly file',
        default = 'code.asm'
    )
    parser.add_argument(
        '--data_dump_file',type = str,
        help = 'Specify output data file',
        default = 'datamem.txt'
    )
    parser.add_argument(
        '--inst_dump_file',type = str,
        help = 'Specify output inst file',
        default = 'instmem.txt'
    )
    parser.add_argument(
        '--max_instructions',type = int,
        help = 'Specify max number of instructions',
        default = 1024
    )
    parser.add_argument(
        '--verbose',action = 'store_true',
        help = 'Print verbose conversions',
    )
    args = parser.parse_args()
    main(
        isa_file = args.isa_file,
        asm_file = args.asm_file,
        data_dump_file = args.data_dump_file,
        inst_dump_file = args.inst_dump_file,
        max_instructions = args.max_instructions,
        verbose = args.verbose
    )
