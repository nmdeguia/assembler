# Assembler
An assembler using Python 3 to build machine code for the RISC-V implementation of 32-bit Pipelined Processor in Verilog

Version 3.2:
  - Optional argument parameters
  - Added --max_instructions argument option
  - Added --verbose argument option
  - Fixed error on negative numbers in data memory

Version 3.1:
  - Changed assembly coding to MIPS style for LW and SW

Version 3.0:
  - Added data memory support
  - Added support for filling the output files with zeros
  - Revised shell script usage

Version 2.2:
  - Resolved wrong parsing of registers numbering 10 and above

Version 1.0-0:
  - Only Supports LW and SW

# Usage:
Install python3

Open terminal in assembler directory and run

`$ python assembler.py --argument value`

Arguments:

`--isa_file str_value` - Specifies isa file, default is riscv.isa

`--asm_file str_value` - Specifies assembly file, default is code.asm

`--data_dump_file str_value` - Specifies output data file, default is datamem.txt

`--inst_dump_file str_value` - Specifies output inst file, default is instmem.txt

`--max_instructions int_value` - Specifies maximum number of instructions, default is 1024

`--verbose` - Show all conversions line by line

# Assembly Code:
Syntax for assembly code:

| Code | Description |
| -- | -- |
| `NOP` | [no operation] |
| `LW/SW $RD 0xIMM($RS)` | [operation $destination_register 0ximmediate ( $source_register ) ] |
| `ADD $RD $RS1 $RS2` | [operation $destination_register $source_register1 $source_register2] |
| `SUB $RD $RS1 $RS2` | |
| `ADDI $RD $RS1 0xIMM` | [operation $destination_register $source_register1 0ximmediate] |
| `SLTI $RD $RS1 0xIMM` | |
| `BEQ $RS1 $RS2 0xIMM`| [operation $source_register1 $source_register2 0ximmediate] |
| `BNE $RS1 $RS2 0xIMM` | |
| `J 0xIMM` | [operation 0ximmediate] |
| `JAL 0xIMM` | |
| `JR $RD` | [operation $destination_register] |

Please refer to the Instruction Set List.png for a complete list of instructions in tabular form along with their corresponding bit placement.

Here is the RISCV instruction list table supported in this assembler:
![RISCV instruction set](https://github.com/nmdeguia/assembler/blob/master/isa.png?raw=true)
